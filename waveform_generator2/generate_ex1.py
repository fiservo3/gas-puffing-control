import waveform

wf=waveform.waveform()

#wf.pulse(0 , duration = 42, amplitude = 0.1)
#wf.pulse(10, duration = 42, amplitude = 0.1)
#wf.pulse(20, duration = 42, amplitude = 0.1)
#wf.pulse(30, duration = 42, amplitude = 0.1)
#wf.pulse(40, duration = 42, amplitude = 0.1)

wf.pulse(5 , duration = 0.1, amplitude = 0.8)
wf.pulse(6 , duration = 0.1, amplitude = 0.8)
wf.pulse(7 , duration = 0.1, amplitude = 0.8)
wf.pulse(8 , duration = 0.1, amplitude = 0.8)
wf.pulse(9 , duration = 0.1, amplitude = 0.8)

wf.generate(filename="../waveform.txt")