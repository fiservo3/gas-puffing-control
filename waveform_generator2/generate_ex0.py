import waveform

wf=waveform.waveform()


wf.pulse(10 , duration = 2, amplitude = 0.8)
wf.pulse(15, duration = 0.6, amplitude = 0.25)

wf.plot()

wf.generate(filename="../waveform.txt")