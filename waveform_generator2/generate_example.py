import waveform
import math


#Create new waveform
wf_1=waveform.waveform()


koef=0.3
def f(x):
	return 0.2*math.sin(x)






#Firt_we wil use pusles
wf_1.pulse(1, 0.2, 2*koef)	#short spike at begining
wf_1.pulse(10, 15, koef)	#For sine wave to superpose onto

wf_1.ramp(2, 2, start_amplitude=0.1, end_amplitude=0.4)

wf_1.function(start=10, duration=2*math.pi, func=f)

wf_1.generate("./waveform.txt")
wf_1.plot()

