import numpy as np
import scipy

import string
import os
import shutil

from scipy.interpolate import interp1d
from matplotlib import pyplot as plt



class waveform():
	def __init__(self, samples = 16384, duration = 100):
		self.samples = samples
		self.duration = duration

		self.waveform = np.zeros(samples)

	def sample(self, time):
		x = (time * self.samples / self.duration)
		return(int(round(x)))

	def time(self, sample):
		return(sample*self.duration/self.samples)

	def pulse(self, start, duration, amplitude):
		beg_sample = self.sample(start)
		end_sample = self.sample(start+duration)

		if end_sample <= beg_sample:
			print("error: use longer pulse")
			return

		if beg_sample < 0:
			print("warning: negative time")
			beg_sample = 0

		if end_sample >= self.samples:
			print("warning: over time")
			end_sample = self.samples-1

		print("pulse: samples " + str(beg_sample) + " to " + str(end_sample))

		for i in range(beg_sample, end_sample):
			self.waveform[i] += amplitude


	def function(self, start, duration, func):
		beg_sample = self.sample(start)
		end_sample = self.sample(start+duration)

		if end_sample <= beg_sample:
			print("error: use longer pulse")
			return

		if beg_sample < 0:
			print("warning: negative time")
			beg_sample = 0

		if end_sample >= self.samples:
			print("warning: over time")
			end_sample = self.samples-1

		for i in range(beg_sample, end_sample):
			t=self.time(i)-start
			self.waveform[i] += func(t)


	def ramp(self, start, duration, start_amplitude=0, end_amplitude = 1):
		beg_sample = self.sample(start)
		end_sample = self.sample(start+duration)

		if end_sample <= beg_sample:
			print("error: use longer ramp")
			return

		if beg_sample < 0:
			print("warning: negative time")
			beg_sample = 0

	
		#print("pulse: samples " + str(beg_sample) + " to " + str(end_sample))
		ramp_samples = np.linspace(start_amplitude, end_amplitude, num=end_sample - beg_sample)
		j=0
		for i in range(beg_sample, end_sample):
			self.waveform[i] += ramp_samples[j]
			j+=1





	def generate(self, filename="./waveform.txt"):
		outfile = open(filename, "w")

		self.waveform[0] = 0.
		self.waveform[-1] = 0.

		for i in range(self.samples):
			value = self.waveform[i]
			if value < 0:
				print("warning: sample lower than zero")
				value = 0

			if value > 1:
				print("warning: sample higher than one")
				value = 1

			outfile.write(str(value) + "\n")

	def plot(self, begin=0, end=30):

		t = np.linspace(0, 100, num=self.samples)

		beg_sample = self.sample(begin)
		end_sample = self.sample(end)

		plt.plot(t[beg_sample: end_sample], self.waveform[beg_sample: end_sample])
		plt.xlabel("t [ms]")
		plt.ylabel("u")
		plt.show()





