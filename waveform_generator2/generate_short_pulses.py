import waveform
import numpy as np

wf=waveform.waveform()

for i in np.linspace(5, 15, num=10):
	wf.pulse(i, duration=0.2, amplitude=0.8)

wf.plot()

wf.generate(filename="../waveform.txt")