#IP  = 192.168.2.79
#IP  = 192.168.1.195
IP  = 192.168.2.86
waveform = ./waveform.txt

all: generate transfer load

load_bitstream:
	ssh golem@$(IP) 'sudo su -c "cat /opt/redpitaya/fpga/fpga_0.94.bit > /dev/xdevcfg"'

generate:
	python ./generate_waveform.py

transfer:
	scp ./waveform.txt golem@$(IP):~/GasPuffing/

download:
	scp golem@$(IP):~/GasPuffing/out.txt ./

load: 
	ssh golem@$(IP) 'cd GasPuffing && sudo su -c "LD_LIBRARY_PATH=/opt/redpitaya/lib ./puffing"'

load_waveform: 
	scp $(waveform) golem@$(IP):~/GasPuffing/


relay_on: 
	ssh golem@$(IP) 'cd GasPuffing && sudo su -c "LD_LIBRARY_PATH=/opt/redpitaya/lib ./relay_on"'

relay_off: 
	ssh golem@$(IP) 'cd GasPuffing && sudo su -c "LD_LIBRARY_PATH=/opt/redpitaya/lib ./relay_off"'

relay_blink: 
	ssh golem@$(IP) 'cd GasPuffing && sudo su -c "LD_LIBRARY_PATH=/opt/redpitaya/lib ./relay_blink"'

charging: 
	ssh golem@$(IP) 'cd GasPuffing && ./charging.sh'

arming: 
	ssh golem@$(IP) 'cd GasPuffing && ./arming.sh'

after: 
	ssh golem@$(IP) 'cd GasPuffing && ./after.sh'

calib:
	ssh golem@$(IP) 'cd GasPuffing && ./calib.sh'

plot_pressure:
	python plot_pressure.py

calibration: calib download plot_pressure

build_calib:
	ssh golem@$(IP) 'cd examples && make calibration && cp calibration ../GasPuffing/calib'

mount:
	mkdir ./RedPitaya
	sshfs  -o transform_symlinks,follow_symlinks -o reconnect -o Compression=no golem@$(IP): ./RedPitaya

plot:
	generate
	python ./plot_waveform.py

ssh:
	ssh golem@$(IP)